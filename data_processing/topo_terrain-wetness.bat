REM Batch script to generate topographic and terrain wetness variables from Arctic DEM (NOTE: not all variables used in Chardon et al.)
REM Jakob Assmann j.assmann@bios.au.dk March 2020

REM Run on Windows 2012 R2 Standard using OSGEOS with gdal GDAL 2.2.4 (released 2018/03/19)
REM and SAGA GIS Version: 2.3.2 (64 bit, LTR)

REM Download the Arctic DEM tiles for study region at 2 m resoltution - not used in the end!
REM --------------------------

C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/12_37/12_37_1_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_37_1_2_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/12_37/12_37_2_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_37_2_2_2m_v3.0.tar.gz')

C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/12_38/12_38_1_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_38_1_2_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/12_38/12_38_2_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_38_2_2_2m_v3.0.tar.gz')

C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_37/13_37_1_1_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_37_1_1_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_37/13_37_1_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_37_1_2_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_37/13_37_2_1_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_37_2_1_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_37/13_37_2_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_37_2_2_2m_v3.0.tar.gz')

C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_38/13_38_1_1_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_38_1_1_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_38/13_38_1_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_38_1_2_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_38/13_38_2_1_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_38_2_1_2m_v3.0.tar.gz')
C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/2m/13_38/13_38_2_2_2m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\13_38_2_2_2m_v3.0.tar.gz')

REM Repeat for 10 m resolution - not used in the end!
REM C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/10m/12_38/12_38_10m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_38_10m_v3.0.tar.gz')

REM Repeat for 30 m resolution - not used in the end!
REM C:\Windows\System32\WindowsPowerShell\v1.0\PowerShell.exe (new-object System.Net.WebClient).DownloadFile('http://data.pgc.umn.edu/elev/dem/setsm/ArcticDEM/mosaic/v3.0/32m/12_38/12_38_32m_v3.0.tar.gz','D:\Jakob\ArcticDEM\nathalie_nuuk\12_38_32m_v3.0.tar.gz')


REM Extract using 7zip
REM --------------------------

"C:\Program Files\7-Zip\7z" e -y *2m_v3.0.tar.gz 
"C:\Program Files\7-Zip\7z" e -y *2m_v3.0.tar -o.\2m

REM "C:\Program Files\7-Zip\7z" e -y *10m_v3.0.tar.gz 
REM "C:\Program Files\7-Zip\7z" e -y *10m_v3.0.tar -o.\10m\dem_files

REM "C:\Program Files\7-Zip\7z" e -y *32m_v3.0.tar.gz 
REM "C:\Program Files\7-Zip\7z" e -y *32m_v3.0.tar -o.\32m\dem_files

REM Calculate TWI for 2 m DEM
REM --------------------------
cd 2m 
dir *_dem.tif /s /b > list_of_files.txt
C:\OSGeo4W64\OSGeo4W.bat gdalbuildvrt -input_file_list D:\Jakob\ArcticDEM\nathalie_nuuk\list_of_files.txt D:\Jakob\ArcticDEM\nathalie_nuuk\ArcticDEM_nuuk.vrt
C:\OSGeo4W64\apps\saga-ltr\saga_cmd.exe ta_hydrology 15 -DEM D:\Jakob\ArcticDEM\nathalie_nuuk\ArcticDEM_nuuk.vrt -TWI D:\Jakob\ArcticDEM\nathalie_nuuk\SAGA_TWI_nuuk.sdat

REM Get CRS, extent and resolution from main raster
C:\OSGeo4W64\OSGeo4W.bat gdalsrsinfo -o wkt O:\ST_Ecoinformatics\C_Write\_User\NathalieChardon_au653181\input_data\processed\main_ras.tif > D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt
C:\OSGeo4W64\OSGeo4W.bat gdalinfo O:\ST_Ecoinformatics\C_Write\_User\NathalieChardon_au653181\input_data\processed\main_ras.tif > D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target_info.txt

REM Reproject raster into CRS, extent and resolution using gdalwarp
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\2m\SAGA_TWI_nuuk.sdat D:\Jakob\ArcticDEM\nathalie_nuuk\SAGA_TWI_nuuk_ArcticDEM.tif

REM Calculate TWI for GIMP 90 m dem
REM --------------------------
REM Crop Greenland-wide DEM to 100 km buffer around study region:
gdal_translate -projwin -453305.0 -2639995.0 -134595.0 -2937015.0 -a_nodata -9999.0 -ot Float32 -of GTiff O:/ST_Ecoinformatics/B_Read/Arctic/Greenland/dem/gimpdem_90m.tif D:/Jakob/ArcticDEM/nathalie_nuuk/gimp_90m_crop_100km_buffer.tif

REM Calculate slope
C:\OSGeo4W64\OSGeo4W.bat gdaldem slope D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer.tif D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer_slope.tif
REM Reproject and clip:
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer_slope.tif D:\Jakob\ArcticDEM\nathalie_nuuk\Slope_nuuk.tif

REM Calculate aspect
C:\OSGeo4W64\OSGeo4W.bat gdaldem aspect -zero_for_flat D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer.tif D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer_aspect.tif
REM Reproject and clip:
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer_aspect.tif D:\Jakob\ArcticDEM\nathalie_nuuk\Aspect_nuuk.tif

REM Generate SAGA wetness index
C:\OSGeo4W64\apps\saga-ltr\saga_cmd.exe ta_hydrology 15 -DEM D:\Jakob\ArcticDEM\nathalie_nuuk\gimp_90m_crop_100km_buffer.tif -TWI D:\Jakob\ArcticDEM\nathalie_nuuk\SAGA_TWI_nuuk_gimpdem.sdat
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\SAGA_TWI_nuuk_gimpdem.sdat D:\Jakob\ArcticDEM\nathalie_nuuk\SAGA_TWI_nuuk_gimpdem.tif

REM Sentinel NDWI, NDVI, B8 and B11 from GEE
REM --------------------------
REM See script: https://code.earthengine.google.com/ae05f4fd60bafc353cbf2fc092761e70

REM Reproject NDWI raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\NDWI_nuuk_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\NDWI_nuuk.tif

REM Reproject NDVI raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\NDVI_nuuk_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\NDVI_nuuk.tif

REM Reproject Sentinel B8 raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelB8Anir_nuuk_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelB8Anir_nuuk.tif

REM Reproject Sentinel B11 raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelB11swir_nuuk_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelB11swir_nuuk.tif

REM TCwetness from Sentinel2 and Landsat from GEE
REM --------------------------
REM For S2 see script above.
REM For Landsat see:https://code.earthengine.google.com/e995697be92b2398fe0c0ee0755beda2

REM Reproject Sentinel TCwet raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelTCwet_nuuk_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\sentinelTCwet_nuuk.tif

REM Reproject Landsat TCwet raster into CRS, extent and resolution using gdalwarp (GEE export)
C:\OSGeo4W64\OSGeo4W.bat gdalwarp -t_srs D:\Jakob\ArcticDEM\nathalie_nuuk\2m\target.wkt -te -353305.000 -2837015.000 -234595.000 -2739995.000 -tr 90 90 -r bilinear -overwrite -wo NUM_THREADS=54 D:\Jakob\ArcticDEM\nathalie_nuuk\landsatTCwet_NUUK_UTM22.tif D:\Jakob\ArcticDEM\nathalie_nuuk\landsatTCwet_nuuk.tif


