## N. Chardon
## Start: 21 Oct 2020
## Aims: 
## 1. Construct BRTs with BeNa/SaGl presence-absence data for SDMs (train: Nuup Kangerlua)
## 2. Project to landscape (test: Kangerluarsunnguaq)
## 3. Extract to grids DF

## NC NOTE: file revised from brt_pa_mods.R


rm(list=ls())


## LIBRARIES ##

library(sf)
library(dismo)
library(gbm)
library(raster)
library(ape) #Moran's I


## WORKING DIRECTORIES ##

# Environmental data input
temp <- '/Volumes/Nat_Ecoinformatics/B_Read/Arctic/Microclimate/NuukFjord/FINAL_LST_TS_PAST/'
precip <- '/Volumes/Nat_Ecoinformatics/B_Read/Arctic/Microclimate/NuukFjord/FINAL_Precipitation_TS_PAST/'
ins <- '/Volumes/Nat_Ecoinformatics/B_Read/Arctic/Microclimate/NuukFjord/FINAL_SolarRadiation/'
topo <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/input_data/topographic/'

# Output rasters
out.ras <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/input_data/processed/'
pred.ras <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/input_data/pred_rasters/'

# Output models
mod <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/model_outputs/'

# Local directory independent of AU server
loc <- '/Volumes/MILKBONE/Greenland/processed_data/'
loc.dat <- '~/Desktop/Research/Greenland/shrubs/RData/'

# Plot-level Rdata  
my.dat <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/scripts/RData/'

# Figures
figs <- '/Volumes/Nat_Ecoinformatics/C_Write/_User/NathalieChardon_au653181/figs/brt_compare_Nov2020/'


# # DATA INPUT # #
setwd(my.dat) 
load('ts_grid_avg.RData') #env + sp data at grid level, org. created in brt_mods.R (NF_data.R) -- NOT HOSTED


# # DATA OUTPUT # #
setwd(my.dat) 
load('ts_grid_avg.RData') #env + sp data at grid level, org. created in brt_mods.R (NF_data.R; updated brt_pa_mods.R)

setwd(my.dat)
load('var_cont_pa_BeNa.RData') #variable contribution out of 10 BRT runs for BeNa (brt_pa_mods.R)
load('var_cont_pa_SaGl.RData') #variable contribution out of 10 BRT runs for SaGl (brt_pa_mods.R)

setwd(mod)
load('brt_bena_pa.rda') #P-A BRT model for Betula nana (brt_pa_mods.R)
load('brt_sagl_pa.rda') #P-A BRT model for Betula nana (brt_pa_mods.R)

setwd(pred.ras)
preds <- raster('BeNa_pred_pa.tif') #BeNa P-A projection (brt_pa_mods.R)
preds <- raster('SaGl_pred_pa.tif') #SaGl P-A projection (brt_pa_mods.R)

setwd(loc.dat)
load('gridsP_avg_pa.RData') #testing data with predictors and all three sets of BRT predictions (brt_pa_mods.R)




####################################################################################################

# # DEFINE VARIABLES # #

####################################################################################################

# # Create binary response variable
ts_grid$pa_bena <- NA # Betula nana P-A points
ts_grid[which(ts_grid$pins_bena > 0),]$pa_bena <- 1
ts_grid[which(ts_grid$pins_bena == 0),]$pa_bena <- 0


ts_grid$pa_sagl <- NA # Salix glauca P-A points
ts_grid[which(ts_grid$pins_sagl > 0),]$pa_sagl <- 1
ts_grid[which(ts_grid$pins_sagl == 0),]$pa_sagl <- 0


# Save DF
setwd(my.dat) 
save(ts_grid, file = 'ts_grid_avg.RData') #env + sp data at grid level, org. created in brt_mods.R (NF_data.R)


dat <- ts_grid

# Keep only predictor variables of interest

dat <- dat[, c(1:8, 75, #metadata, p-a data
               19:20, 25, 30, 35, 40, 61, #climate data at 30 years (exclude precipjfmam)
               44:46, 62)] #topo data

colnames(dat)
start <- 10 #first column of env predictors


# Check predictor correlation
cor(dat[start:ncol(dat)])



####################################################################################################

# # CHOOSE BEST BAGGING FRACTION (TRAINING + TESTING DATASET) # # 
# => 0.7 has highest pred power for BeNa
# => 1 has highest pred power for SaGl

####################################################################################################

# Partition DF
train <- sample(nrow(dat), 2/3*nrow(dat)) #randomly sample 2/3 of DF

dat_train <- dat[train, ] #create training and testing DFs
dat_test <- dat[-train, ]


# Define variables and parameters
# yy <- 'pa_bena' #response variable
yy <- 'pa_sagl' #response variable
xx <- colnames(dat[c(start:ncol(dat))]) #all env vars
# lr <- 0.001 #BeNa learning rates 
lr <- 0.0005 #Sagl learning rates 

# Change bagging fractions (0.4 - 0.9)
# bf <- 0.7 #BeNa
bf <- 1 #SaGl

# Run BRT
mod.bf <- gbm.step(data=dat_train, gbm.x = xx,
                   gbm.y = yy, family = 'bernoulli', tree.complexity = 3, 
                   learning.rate = lr, bag.fraction = bf, 
                   n.trees = 100) #increase trees to start at & add to each cycle to prevent nonconvergece 


# Assess model performance with testing data
pred <- predict(mod.bf, dat_test, mod.bf$gbm.call$best.trees) #prediction with ntrees

cor(pred, dat_test[yy]) #correlation of predicted vs. training data

# BeNa =>
# bf 0.4 (cor, CV AUC): 0.57, 0.77
# bf 0.5: 0.62, 0.76
# bf 0.6: 0.64, 0.74
# bf 0.7: 0.67, 0.69
# bf 0.8: 0.66, 0.68
# bf 0.9: 0.59, 0.77

# SaGl =>
# bf 0.4 (cor, CV AUC, RMSE): 0.29, 0.622, 0.43
# bf 0.5: 0.30, 0.60, 0.43
# bf 0.6: 0.31, 0.57, 0.44
# bf 0.7: 0.32, 0.59, 0.32
# bf 0.8: 0.33, 0.57, 0.42
# bf 0.9: 0.33, 0.58, 0.41
# bf 1: 0.55, 0.629, 0.55

mean(abs(dat_test[,yy] - pred)) #mean absolute error

plot(dat_test[,yy], pred)




####################################################################################################

# # ID BEST LEARNING RATE FOR TREES CLOSEST TO 1000 AND ID VARIABLES TO DROP # # 
# # (FULL DATASET OF SPATIAL/LONG-TERM VARS)

####################################################################################################

# sources: https://rspatial.org/raster/sdm/9_sdm_brt.html#spatial-prediction
# Elith et al. 2008 appendix)

# Identify lr n.trees > 1000 
# yy <- 'pa_bena' #response variable
yy <- 'pa_sagl' #response variable
xx <- colnames(dat[c(start:ncol(dat))]) #all env vars
# bf <- 0.7 #best BF for BeNa as identified above
bf <- 1 #best BF for SaGl as identified above
# lr <- 0.002 #best learning rate for BeNa
lr <- 0.0005 #best learning rate for SaGl

mod.trees <- gbm.step(data=dat, gbm.x = xx,
                      gbm.y = yy, family = 'bernoulli', tree.complexity = 3, 
                      learning.rate = lr, bag.fraction = bf) 

# Store variable contribution out of 10 runs
# var_cont <- summary(mod.trees)[, 1]
var_cont <- cbind(var_cont, summary(mod.trees)[, 1])

colnames(var_cont) <- c('run1', 'run2', 'run3', 'run4', 'run5', 'run6', 'run7', 'run8', 'run9', 'run10')
var_cont

setwd(my.dat)
# save(var_cont, file = 'var_cont_pa_BeNa.RData')
save(var_cont, file = 'var_cont_pa_SaGl.RData')


# Identify number of variables to drop
mod.drop <- gbm.simplify(mod.trees1, n.drops = 9) #can drop all but 2 variables

# sum(3, 7, 7, 1, 7, 7, 7, 7, 2, 7)/10 # BeNa = remove 6, keep 5 (avg of 10 runs)
sum(8, 4, 4, 9, 9, 4, 4, 9, 9, 4)/10 # SaGl = remove 6, keep 5 (avg of 10 runs)


# # BeNa: Run simplified model 
# xx <- summary(mod.trees)[1:5, 1] #these preds stable across all 10 BRT runs from above
# bf <- 0.7 #best BF for BeNa as identified above
# lr <- 0.002 #best learning rate for BeNa

# SaGl: Run simplified model 
xx <- summary(mod.trees)[1:5, 1] #these preds stable across all 10 BRT runs from above
bf <- 1 #best BF for SaG; as identified above
lr <- 0.0007 #best learning rate for SaGl (adjusted here to decrease number of trees)

mod.simp <- gbm.step(data=dat, gbm.x = xx,
                     gbm.y = yy, family = 'bernoulli', tree.complexity = 3, 
                     learning.rate = lr, bag.fraction = bf)
summary(mod.simp)


# Save model outputs
setwd(mod)
# brt_bena_pa <- mod.simp
# save(brt_bena_pa, file = 'brt_bena_pa.rda') 
brt_sagl_pa <- mod.simp
save(brt_sagl_pa, file = 'brt_sagl_pa.rda')


# # SAC IN BRT RESIDUALS # #
# Note: SAC in residuals won't be found if explanatory variables account for spatial structure; 
# could include coordinates as variable to account for spatial structure

dists <- as.matrix(dist(cbind(dat$long, dat$lat))) #distance matrix
dists.inv <- 1/dists #inverse distance matrix
diag(dists.inv) <- 0 #diagonals = 0

dists.inv[1:5, 1:5]

rr <- resid(mod.simp)
Moran.I(rr, dists.inv) #no spatial autocorrelation in BeNa or SaGl




####################################################################################################

# # BeNa P-A: PREDICT TO LANDSCAPE (revised from brt_compare.R) # #

####################################################################################################

# # MODEL TO PREDICT TO # # 
setwd(mod)
load('brt_bena_pa.rda') 

nt <- brt_bena_pa$gbm.call$best.trees #number of trees from model above
xx <- summary(brt_bena_pa)[,1] #predictor names


# # LOAD PREDICTOR RASTERS # #
setwd(topo)
slope_90m <- raster('Slope_nuuk.tif')

setwd(pred.ras)
tempmin <- raster('tempmin.asc') #avg minimum temp 1984-2013 (projections.R)
mamprecip <- raster('mamprecip.asc') #avg cumulative MAM precip 1984-2013 (projections.R)
tempcont <- raster('tempcont.asc') #avg max - min temp 1984-2013 (projections.R)
insol <- raster('insol.asc') #avg JJA insolation 1984-2013 (projections.R)

pred_stack <- stack(slope_90m, insol, mamprecip, tempmin, tempcont) #stack all rasters
names(pred_stack) <- (c('slope_90m', 'insoljja_ts_30', 'precipmam_ts_30', 
                        'tempmin_ts_30', 'tempcont_ts_30'))

# Mask water 
setwd(out.ras)
dem <- raster('dem_crop.tif') #main file for DEM raster (ts_90m.R; NC)

dem[dem <= 33] <- NA #sea level = 33 m
pred_stack <- mask(pred_stack, dem) #copies all NA values onto preds, all other values not changed


# # BRT: PREDICT TO STACK # #
p <- predict(pred_stack, brt_bena_pa, n.trees = nt, type = 'response')
plot(p)

crs(p) <- '+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=1 +y_0=1 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0'

# Mask water 
p <- mask(p, dem) #copies all NA values onto preds, all other values not changed
plot(p)

# Save prediction raster
setwd(pred.ras)
writeRaster(p, filename = 'BeNa_pred_pa.tif', overwrite = T) 




####################################################################################################

# # SaGl P-A: PREDICT TO LANDSCAPE (revised from brt_compare.R) # #

####################################################################################################

# # MODEL TO PREDICT TO # # 
setwd(mod)
load('brt_sagl_pa.rda') 

nt <- brt_sagl_pa$gbm.call$best.trees #number of trees from model above
xx <- summary(brt_sagl_pa)[,1] #predictor names

cor(dat[xx]) #tempmin and tempcont correlated (-0.8)


# # LOAD PREDICTOR RASTERS # #
setwd(topo)
asp_90m <- raster('Aspect_nuuk.tif')
tcws <- raster('sentinelTCwet_nuuk.tif')
twi_90m <- raster('SAGA_TWI_nuuk_gimpdem.tif')

setwd(pred.ras)
tempmin <- raster('tempmin.asc') #avg minimum temp 1984-2013 (projections.R)
tempcont <- raster('tempcont.asc') #avg max - min temp 1984-2013 (projections.R)

pred_stack <- stack(asp_90m, tcws, twi_90m, tempmin, tempcont) #stack all rasters
names(pred_stack) <- (c('asp_90m', 'tcws', 'twi_90m', 'tempmin_ts_30', 'tempcont_ts_30'))

# Mask water 
setwd(out.ras)
dem <- raster('dem_crop.tif') #main file for DEM raster (ts_90m.R; NC)

dem[dem <= 33] <- NA #sea level = 33 m
pred_stack <- mask(pred_stack, dem) #copies all NA values onto preds, all other values not changed


# # BRT: PREDICT TO STACK # #
p <- predict(pred_stack, brt_sagl_pa, n.trees = nt, type = 'response')
plot(p)

crs(p) <- '+proj=stere +lat_0=90 +lat_ts=70 +lon_0=-45 +k=1 +x_0=1 +y_0=1 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0'


# Save prediction raster
setwd(pred.ras)
writeRaster(p, filename = 'SaGl_pred_pa.tif', overwrite = T) 




####################################################################################################

# # EXTRACT BRT MODEL PREDICTIONS (duplicated from brt_compare.R) # # 

####################################################################################################

# # Load data: BeNa extraction
# setwd(loc.dat)
# load('gridsP_avg.RData') #testing data with predictors and both sets of BRT predictions (brt_compare.R)
# setwd(pred.ras)
# preds <- raster('BeNa_pred_pa.tif') 

# Load data: SaGl extraction
setwd(loc.dat)
load('gridsP_avg_pa.RData') #testing data with predictors and both sets of BRT predictions (brt_compare.R)
setwd(pred.ras)
preds <- raster('SaGl_pred_pa.tif') 


# # Crop raster to sampling area (jakob_nuuk_sampling_2020.R)

# Set base locations
base_locations <- data.frame(name = c("GINR", "Kobbefjord"), 
                             lat = c(64.190531, 64.135664), long = c(-51.695124, -51.385006))

base_locations <- st_as_sf(base_locations, coords = c("long", "lat"), crs = 4326)
base_locations <- st_transform(base_locations, crs(preds))

# Crop preds raster with 10 k buffer
preds <- crop(preds, extent(as_Spatial(st_buffer(base_locations, 10000))))
plot(preds)


# # Extract all BRT predictions for each grid
gps <- as.matrix(grids[,c('x_stere', 'y_stere')]) #matrix of long, lat 
grids$pred_pa_sagl <- extract(preds, gps)
summary(grids$pred_pa_sagl)

plot(pred_pa_sagl ~ pred_pa, data = grids)


# Save updated DF
setwd(loc.dat)
save(grids, file = 'gridsP_avg_pa.RData') #testing data with predictors and both sets of BRT predictions



