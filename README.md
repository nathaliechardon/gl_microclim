# gl_microclim

Updated: 20 June 2022

## Summary

R scripts and data corresponding to Chardon et al. 2022. High resolution Species Distribution and Abundance Models cannot predict separate shrub datasets in adjacent Arctic fjords. Diversity and Distributions, 28(5): 956-975. https://doi.org/10.1111/ddi.13498

Scripts run with R versions 4.0.2 and 4.1.1. Data also stored and described in detail on EnviDat (https://www.envidat.ch/dataset/gl_microclim). All data and scripts are open access under license CC-BY-SA. Please contact nathalie.chardon_at_gmail.com with any inquiries. 

## results

Tables and figures in Chardon et al. 2022. Includes all model evaluation steps. 

### ms_figs.R

All main text and Supplementary Information figures.

### ms_figsR.R

All main text and Supplementary Information figures for manuscript revisions.

### ms_tables.R

All main text and Supplementary Information tables. 

### ms_tablesR.R

All main text and Supplementary Information tables for manuscript revisions. Includes predictions to fjord not used in model training, which are stored in the corresponding fjord's dataframe. 


## modeling_revisions

Scripts to construct additional SAMs with GLM using NK and K fjord data to  train and validate each model type in order to compare results with BRTs from 'model_construction'.

### NK_GLM.R

Construct SDMs and SAMs with GLM using NK fjord data.

### K_GLM.R

Construct SDMs and SAMs with GLM using K fjord data.

### calibrate.R

Explore calibration of presence-absence predictions to insert code into ms_tablesR.R.

### env_space_GLM.R

Explore validating models within environmental training space to insert code into ms_tablesR.R.


## model_construction

Scripts to construct i) GLMMs to assess climate sensitivity to different time series lengths, ii) BRTs with NK fjord data for SDMs and SAMs, and iii) BRTs with K fjord data for SDMs and SAMs.

### GLMM.R

GLMMs to assess climate sensitivity to different time series lengths.

### prelim_SAM.R

Construct preliminary SAM (BRT for Betula nana pin sums in NK fjord) and project to landscape. This used to plan field work in K fjord.

### NK_SDM.R

Construct BRTs with Betula nana and Salix glauca presence-absence data from NK fjord. Project BRT to landscape to create SDMs. 

### NK_SAM.R

Construct BRTs with Betula nana and Salix glauca abundance data from NK fjord. Project BRT to landscape to create SAMs. Compare results of Betula nana SAM (average pin counts per grid) with preliminary SAM (summed pin counts per grid).

### K_SDM.R

Construct BRTs with Betula nana and Salix glauca presence-absence data from K fjord. Project BRT to landscape to create SDMs. 

### K_SAM.R

Construct BRTs with Betula nana and Salix glauca abundance data from K fjord. Project BRT to landscape to create SAMs. 


## data_processing

Files in this folder used to process i) original field data, ii) climatic, topographic, and terrain wetness data to the plot and 90 m grid level, iii) create prediction rasters, and iv) design sampling scheme in K fjord. Please see main manuscript and accompanying Supplementary Information for details. Note that data used in these scripts not hosted here, and need to be downloaded from original sources.

### nuuk_dem.R

Crop Greenland DEM to encompass NK and K fjords to use in climate downscaling calculations and other environmental variable calculations.

### downscale_precip.R

Precipitation downscaling calculations by M. Guéguen (maya.gueguen_at_univ-grenoble-alpes.fr).

### downscale_temp_radiation.R

Temperature and solar radiation downscaling calculations by M. Guéguen (maya.gueguen_at_univ-grenoble-alpes.fr). 

### ts_90m.R

Calculations for 30-year downscaled climate time series up to sampling year of NK fjord data.

### topo_terrain-wetness.bat

Batch script detailing topographic and terrain wetness variable calculations by J. Assmann (j.assmann_at_bio.au.dk).

### env_90m.R

Add topographic, terrain wetness, and microscale biotic data to NK fjord data. 

### K_data.R

Create plot-level dataframe for GLMMs and grid-level dataframe for BRTs from K fjord data. Dataframes include all biotic and abiotic predictors as well as species presence-absence and abundance data.

### NK_data.R

Create plot-level dataframe for GLMMs and grid-level dataframe for BRTs from NK fjord data. Dataframes include all biotic and abiotic predictors as well as species presence-absence and abundance data.

### projections.R

Create 30-year climatology predictor rasters (1984 - 2013) for study region to use in all model projections. These rasters then extracted to K fjord sampling locations to construct K fjord SDMs and SAMs.

### k_sampling.R

Create stratified random sampling design for K fjord based on prelim Betula nana SAM constructed with NK fjord pin sums


## data

Files in this folder correspond to processed data used in BRT model (i.e. SDMs and SAMs) construction, and also contain microscale biotic field data.

### k.csv & k.RData

These data collected August 2020 (PI: Nathalie Isabelle Chardon). Please contact N. I. Chardon (nathalie.chardon_at_gmail.com) for individual quadrat (i.e. 1 m x 1 m) data, which are averaged at the 90 m spatial grid here. 

### COLUMN ABBREVIATIONS

#### Metadata for 90 m spatial grid

id: sampling zone and unique 90 m grid identifier

long: WGS84 longitude

lat: WGS84 latitude

x_stere: longitude in Polar Stereographic (EPSG 4326)

y_stere: latitude in Polar Stereographic (EPSG 4326)

date: sampling day.month.year

#### Species presence [1] or absence [0] at grid level

alcr: Alnus crispa

bena: Betula nana

dila: Diapensia lapponica

emni: Empetrum nigrum

hahy: Harimanella hypoides

lopr: Loiseleuria procumbens

oxpa: Oxycoccus palustre

phca: Phyllodoce caerulea

rhod: Rhododendron sp.

saar: Salix arctica

sagl: Salix glauca

sahe: Salix herbacea

vaul: Vaccinium uliginosum

drin: Dryas integrifolia

thpr: Thymus praecox

Habitat characteristics [% cover]

shrub: all shrub species

gram: graminoids

forb: forbs

bryo: bryophytes

lich: lichens

litt: litter

bare: bare ground

stone: stones (> 2 cm)

org: organic crust

#### Shrub species height (maximum vegetative height) [mm] and cover (estimated in 1x1 m quadrat) [%]

alcr_c: Alnus crispa cover

alcr_h: Alnus crispa height

bena_c: Betula nana cover

bena_h: Betula nana height

dila_c: Diapensia lapponica cover

dila_h: Diapensia lapponica height

emni_c: Empetrum nigrum cover

emni_h: Empetrum nigrum height

hahy_c: Harimanella hypoides cover

hahy_h: Harimanella hypoides height

lopr_c: Loiseleuria procumbens cover

lopr_h: Loiseleuria procumbens height

oxpa_c: Oxycoccus palustre cover

oxpa_h: Oxycoccus palustre height

phca_c: Phyllodoce caerulea cover

phca_height: Phyllodoce caerulea height

rhod_c: Rhododendron sp. cover

rhod_h: Rhododendron sp. height

saar_c: Salix arctica cover

saar_h: Salix arctica height

sagl_c: Salix glauca cover

sagl_h: Salix glauca height

sahe_c: Salix herbacea cover

sahe_h: Salix herbacea height

vaul_c: Vaccinium uliginosum cover

vaul_h: Vaccinium uliginosum height

drin_c: Dryas integrifolia cover

drin_h: Dryas integrifolia height

#### Community characteristics

rich_pa: shrub species richness at grid level

rich: shrub species richness averaged across 3 1x1 m sampling quadrats per grid

shan: Shannon diversity (‘vegan’ package; Oksanen et al. 2019)

comp_bena: shrub competition on Betula nana (i.e. summed cover of non-B. nana species) [% cover]

comp_sagl: shrub competition on Salix glauca (i.e. summed cover of non-S. glauca species) [% cover]

ht_cwm: maximum shrub canopy height (i.e. community weighted mean by abundance of maximum shrub height) [mm]

#### Topography and terrain wetness (calculated from)

tcws: Tasseled Cap Wetness component (Sentinal images)

slope: slope angle (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015) [º]

twi: SAGA wetness index (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015)

asp: aspect (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015) [º]

#### Downscaled climate (time series 1984 – 2013)

insoljja: average June-August insolation [MJ cm-2 yr-1]

precipmam: cumulative March-May precipitation [mm]

tempcont: yearly maximum – minimum temperature [ºC]

tempmin: yearly minimum temperature [ºC]

precipjja: cumulative June-August precipitation [mm]

tempjja: average maximum June-August temperature [ºC]

tempmax: yearly maximum temperature [ºC]


### nk.csv & nk.RData

These data collected July-August 2011-2013 (PI: Jacob Nabe-Nielsen). Please contact J. Nabe-Nielsen (jnn_at_ecos.au.dk) to request access to data on all surveyed shrub species. Available here are only data used in Chardon et al. 2022.

### COLUMN ABBREVIATIONS

#### Metadata for 90 m spatial grid

year: sampling year

alt: average altitude of grid as recorded during field surveys [m]

long: WGS84 longitude

lat: WGS84 latitude

x_stere: longitude in Polar Stereographic (EPSG 4326)

y_stere: latitude in Polar Stereographic (EPSG 4326)

#### Species presence [1] or absence [0] at grid level

pa_bena: Betula nana

pa_sagl: Salix glauca

#### Shrub species cover (calculated from pin hits in 0.7 x 0.7 m frame, based on total of 25 hits) [%]

cov_bena: Betula nana cover

cov_sagl: Salix glauca cover

Downscaled climate (time series 1984 – 2013)

tempjja: average maximum June-August temperature [ºC]

tempmax: yearly maximum temperature [ºC]

tempmin: yearly minimum temperature [ºC]

tempcont: yearly maximum – minimum temperature [ºC]

precipmam: cumulative March-May precipitation [mm]

precipjja: cumulative June-August precipitation [mm]

insoljja: average June-August insolation [MJ cm-2 yr-1]

#### Topography and terrain wetness (calculated from)

twi: SAGA wetness index (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015)

slope: slope angle (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015) [º]

asp: aspect (MEaSUREs Greenland Ice Mapping Project Digital Elevation Model v. 1; Howat et al., 2014, 2015) [º]

tcws: Tasseled Cap Wetness component (Sentinal images)

#### Community characteristics

rich: shrub species richness averaged across pin-point frames per grid

shan: Shannon diversity (‘vegan’ package; Oksanen et al. 2019)

comp_bena: shrub competition on Betula nana (i.e. summed cover of non-B. nana species) [% cover]

comp_sagl: shrub competition on Salix glauca (i.e. summed cover of non-S. glauca species) [% cover]